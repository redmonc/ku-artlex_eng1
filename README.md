# KU-ArtLex
## Midwestern American English

### Contributors
Charles Redmon, University of Kansas, redmon@ku.edu  
Seulgi Shin, University of Kansas, seulgi.shin@ku.edu  
Panying Rong, University of Kansas, prong@ku.edu  

### Data Notes

#### Lexicon
At present nearly 20,000 words have been processed of the approximately 27,000 total. Raw data files for the remaining items are provided in the <code>raw_files</code> folder, and will be removed as we process them and shift individual item files to the <code>lexicon</code> folder. To use the raw files, please see the <code>wordlist_lexicon.csv</code> file in the <code>references</code> folder, where block numbers are listed alongside the items they contain. Raw recordings are similarly numbered according to block, with sub-numbering due to occasional breaks in the middle of a block.

#### Syllables
All controlled syllables (CVC, VCV) have been processed and placed into individual item files. Items are transcribed in ARPABET, where the file name is simply the syllable transcription.

#### Audio Data
Audio data is stored in WAV format, and has been sampled at 22050 Hz with 16 bit resolution.

#### EMA Data
EMA data is stored as tab-separated values in plain text files (.tsv extension). Sensor positions are indicated with a T (Tx, Ty, Tz), where Tx is the vertical dimension, Ty is the horizontal dimension, and Tz is the transverse dimension. The columns Tx, Ty, and Tz indicate the reference sensor, and thus should not be used for analysis. The remaining Tx/y/z columns (numbered as Tx.1, Tx.2, etc.) represent the six articulator sensors, which are numbered in the following way:
1. Tongue Dorsum
2. Tongue Center
3. Tongue Tip
4. Upper Lip
5. Lower Lip
6. Jaw
